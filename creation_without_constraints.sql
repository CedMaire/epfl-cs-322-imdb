DROP SCHEMA IF EXISTS imdb;
CREATE SCHEMA imdb;
USE imdb;

CREATE TABLE Spouse (
  id   INT                             NOT NULL,
  name VARCHAR(255) CHARACTER SET utf8 NOT NULL
);


CREATE TABLE Person (
  id       INT                             NOT NULL,
  fullName VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE SpousePerson (
  spouseId INT NOT NULL,
  personId INT NOT NULL
);

CREATE TABLE Biography (
  personId        INT                             NOT NULL,
  realName        VARCHAR(255) CHARACTER SET utf8 NULL,
  nickname        VARCHAR(255) CHARACTER SET utf8 NULL,
  datePlaceBirth  TEXT CHARACTER SET utf8         NULL,
  height          INT                             NULL,
  biography       TEXT CHARACTER SET utf8         NULL,
  biographer      VARCHAR(255) CHARACTER SET utf8 NULL,
  dateCauseDeath  TEXT CHARACTER SET utf8         NULL,
  trivia          TEXT CHARACTER SET utf8         NULL,
  personalQuotes  TEXT CHARACTER SET utf8         NULL,
  salary          VARCHAR(255) CHARACTER SET utf8 NULL,
  trademark       TEXT CHARACTER SET utf8         NULL,
  whereAreTheyNow TEXT CHARACTER SET utf8         NULL
);

CREATE TABLE BioBook (
  id       INT                             NOT NULL,
  title    VARCHAR(255) CHARACTER SET utf8 NOT NULL,
  personId INT                             NOT NULL
);

CREATE TABLE Country (
  id      INT                             NOT NULL,
  country VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Genre (
  id    INT                             NOT NULL,
  genre VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Language (
  id       INT                             NOT NULL,
  language VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Clip (
  id    INT                                             NOT NULL,
  title VARCHAR(255)                 CHARACTER SET utf8 NOT NULL,
  year  INT(4)                                          NULL,
  type  ENUM ('V', 'VG', 'TV', 'SE')                    NULL,
  votes INT                                             NULL,
  rank  FLOAT                                           NULL
);

CREATE TABLE ClipGenres (
  clipId  INT NOT NULL,
  genreId INT NOT NULL
);

CREATE TABLE ClipCountries (
  clipId    INT NOT NULL,
  countryId INT NOT NULL
);

CREATE TABLE ClipLanguages (
  clipId     INT NOT NULL,
  languageId INT NOT NULL
);

CREATE TABLE ClipLink (
  fromId INT                                    NOT NULL,
  toId   INT                                    NOT NULL,
  type   ENUM ('follows',
    'followed by',
    'references',
    'referenced in',
    'features',
    'version of',
    'edited into',
    'edited from',
    'spoofed in',
    'featured in',
    'remake of',
               'spoofs',
               'spin off',
               'remade as',
               'alternate language version of') NOT NULL
);

CREATE TABLE ClipCountryAudience (
  clipId    INT NOT NULL,
  countryId INT
);

CREATE TABLE ReleaseDate (
  clipId    INT  NOT NULL,
  countryId INT,
  date      DATE NOT NULL
);

CREATE TABLE RunningTime (
  clipId      INT NOT NULL,
  countryId   INT,
  runningTime INT NOT NULL
);

CREATE TABLE ActingRole (
  personId    INT                             NOT NULL,
  clipId      INT                             NOT NULL,
  roleName    VARCHAR(255) CHARACTER SET utf8 NULL,
  creditOrder INT                             NULL,
  addInfo     TEXT CHARACTER SET utf8         NULL
);

CREATE TABLE DirectorRole
(
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL
);

CREATE TABLE ProducerRole
(
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL
);

CREATE TABLE WriterRole
(
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  workType VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL
);
