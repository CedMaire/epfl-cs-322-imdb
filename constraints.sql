USE imdb;

ALTER TABLE Spouse
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE Person
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE SpousePerson
  ADD CONSTRAINT Wedding_spouse_fk
FOREIGN KEY (spouseId)
REFERENCES Spouse (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE SpousePerson
  ADD CONSTRAINT Wedding_person_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE Biography
  ADD PRIMARY KEY (personId);
ALTER TABLE Biography
  ADD CONSTRAINT Biography_Person_person_id_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE BioBook
  ADD PRIMARY KEY (id);
ALTER TABLE BioBook
  ADD CONSTRAINT Biobook_biographies_fk
FOREIGN KEY (personId)
REFERENCES Biography (personId)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE Country
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE Genre
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE Language
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE Clip
  ADD PRIMARY KEY (id);
######################################################################
ALTER TABLE ClipLanguages
  ADD CONSTRAINT ClipsLanguages_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ClipLanguages
  ADD CONSTRAINT ClipsLanguages_Language_language_fk
FOREIGN KEY (languageId)
REFERENCES Language (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ClipGenres
  ADD CONSTRAINT ClipGenre_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ClipGenres
  ADD CONSTRAINT ClipGenre_Genre_id_fk
FOREIGN KEY (genreId)
REFERENCES Genre (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ClipCountries
  ADD CONSTRAINT ClipCountry_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ClipCountries
  ADD CONSTRAINT ClipCountry_Country_id_fk
FOREIGN KEY (countryId)
REFERENCES Country (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ClipLink
  ADD CONSTRAINT ClipLink_Clip_from_id_fk
FOREIGN KEY (fromId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ClipLink
  ADD CONSTRAINT ClipLink_Clip_to_id_fk
FOREIGN KEY (toId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ClipCountryAudience
  ADD UNIQUE (clipId, countryId);
ALTER TABLE ClipCountryAudience
  ADD CONSTRAINT ClipCountryAudience_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ClipCountryAudience
  ADD CONSTRAINT ClipCountryAudience_Country_country_fk
FOREIGN KEY (countryId)
REFERENCES Country (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ReleaseDate
  ADD CONSTRAINT ClipCountryAudience_ReleaseDate_fk
FOREIGN KEY (clipId, countryId)
REFERENCES ClipCountryAudience (clipId, countryId)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE RunningTime
  ADD CONSTRAINT ClipCountryAudience_RunningTime_fk
FOREIGN KEY (clipId, countryId)
REFERENCES ClipCountryAudience (clipId, countryId)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
#######################################################################
ALTER IGNORE TABLE ActingRole
  ADD UNIQUE (personId, clipId, roleName, creditOrder, addInfo(200));
ALTER TABLE ActingRole
  ADD CONSTRAINT ActingRole_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ActingRole
  ADD CONSTRAINT ActingRole_Person_id_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE DirectorRole
  ADD UNIQUE (personId, clipId, roleName, addInfo(200));
ALTER TABLE DirectorRole
  ADD CONSTRAINT DirectorRole_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE DirectorRole
  ADD CONSTRAINT DirectorRole_Person_id_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE ProducerRole
  ADD UNIQUE (personId, clipId, roleName, addInfo(200));
ALTER TABLE ProducerRole
  ADD CONSTRAINT ProducerRole_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE ProducerRole
  ADD CONSTRAINT ProducerRole_Person_id_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
######################################################################
ALTER TABLE WriterRole
  ADD UNIQUE (personId, clipId, roleName, addInfo(200));
ALTER TABLE WriterRole
  ADD CONSTRAINT WriterRole_Clip_id_fk
FOREIGN KEY (clipId)
REFERENCES Clip (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;
ALTER TABLE WriterRole
  ADD CONSTRAINT WriterRole_Person_id_fk
FOREIGN KEY (personId)
REFERENCES Person (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE;