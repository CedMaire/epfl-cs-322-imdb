function encodeQueryData(data) {
    let ret = [];
    for (let d in data)
    ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    return ret.join('&');
}

function toggleLoad(bool) {
    let elem = document.getElementById("loader");
    elem.hidden = !bool;
}

function displayError(message) {
    let elem = document.getElementById("message-error");
    elem.innerText = message;
    elem.hidden = false;
}

function removeError() {
    document.getElementById("message-error").hidden = true;
}

function displayMessage(message) {
    let elem = document.getElementById("message-info");
    elem.innerText = message;
    elem.hidden = false;
}

function removeMessage() {
    document.getElementById("message-info").hidden = true;
}

function fillTable(array, tableId, hasMoreInfos=false, isPerson=false) {
    let table = document.getElementById(tableId);
    table.hidden = false;
    
    if (tableId === "results") {
        let head = "<tr><th>#</th>";
        // Fill in header
        Object.keys(array[0]).forEach (function (key) {
            head += "<th>" + key + "</th>";
        });
        head += "</tr>";
        table.getElementsByTagName("thead")[0].innerHTML = head;
    }
    
    // Fill in body
    let body = "";
    let i = 1;
    array.forEach (function (row) {
        if (hasMoreInfos) {
            body += "<tr data-toggle=\"modal\" data-target=\"#moreinfos\" onclick=\"updateInfos(" + 
            row.id + "," + isPerson + ")\"><td>" + i + "</td>";
        } else {
            body += "<tr><td>" + i + "</td>";
        }
        Object.values(row).forEach(function (cell) {
            body += "<td>" + cell + "</td>";
        });
        body += "</tr>";
        i++;
    })
    
    table.getElementsByTagName("tbody")[0].innerHTML = body;
}

function cleanTable() {
    document.getElementById("results").hidden = true;
    document.getElementById("clips-wrap").hidden = true;
    document.getElementById("actors-wrap").hidden = true;
    document.getElementById("directors-wrap").hidden = true;
    document.getElementById("producers-wrap").hidden = true;
    document.getElementById("writers-wrap").hidden = true;
}

function getPredefined(str) {
    cleanTable();
    removeError();
    removeMessage();
    toggleLoad(false);
    let params = {
        query_id : str,
    };
    
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            toggleLoad(false);
            console.debug(this.response);
            let results = JSON.parse(this.response);
            console.debug(results);
            if (results.error != null) {
                displayError(results.error);
            } else {
                displayMessage("Query took " + results.time + " s.");
                fillTable(results.result, 'results');
            }
        } else {
            
        }
    };
    xmlhttp.open("GET","predefined.php?" + encodeQueryData(params, true));
    xmlhttp.send();
    toggleLoad(true);
}

function insert(params) {
    removeError();
    removeMessage();
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.debug(this.response);
            let results = JSON.parse(this.response);
            console.debug(results);
            if (results.error != null) {
                displayError(results.error);
            } else {
                displayMessage("Insertion complete with generated ID " + results.id + " (" + results.time + " s.)");
            }
        }
    };
    xmlhttp.open("POST", "insert.php", true);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xmlhttp.send(encodeQueryData(params));
}

function delete_(params) {
    removeError();
    removeMessage();
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            console.debug(this.response);
            let results = JSON.parse(this.response);
            console.debug(results);
            if (results.error != null) {
                displayError(results.error);
            } else if (results.rows == 1) {
                displayMessage("Delete complete ("  + results.time + " s.)");
            } else if (results.rows == 0) {
                displayMessage("Nothing was deleted ("  + results.time + " s.)");
            }
        }
    };
    xmlhttp.open("DELETE","delete.php?" + encodeQueryData(params));
    xmlhttp.send();
}

function updateInfos(clipId, isPerson) {
    removeError();
    removeMessage();
    xmlhttp = new XMLHttpRequest();
    if (isPerson) {
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let results = JSON.parse(this.response);
                if (results.error != null) {
                    displayError(results.error);
                } else {
                    let r = results.result;
                    document.getElementById("infos-title").innerText = r.fullName;
                    text = "";
                    if (r.biography) text += r.biography + "<hr/><br/>";
                    if (r.realName) text += "<b>Real name: </b>" + r.realName + "<br/>";
                    if (r.nickname) text += "<b>Nick name: </b>" + r.nickname + "<br/>";
                    if (r.datePlaceBirth) text += "<b>Birth: </b>" + r.datePlaceBirth + "<br/>";
                    if (r.dateCauseDeath) text += "<b>Death: </b>" + r.dateCauseDeath + "<br/>";
                    if (r.height) text += "<b>Height: </b>" + r.height + "cm<br/>";
                    if (r.spouses) text += "<b>Spouses: </b>" + r.spouses + "<br/>";
                    if (r.biobooks) text += "<b>Bio books: </b>" + r.biobooks + "<br/>";
                    if (r.actings) text += "<b>Clips acted in: </b>" + r.actings + "<br/>";
                    if (r.directings) text += "<b>Clips directed: </b>" + r.directings + "<br/>";
                    if (r.producings) text += "<b>Clips produced: </b>" + r.producings + "<br/>";
                    if (r.writings) text += "<b>Clips writed: </b>" + r.writings + "<br/>";
                    document.getElementById("modal-text").innerHTML = text;
                }
            }
        };
        xmlhttp.open("GET", "person_infos.php?id=" + clipId, true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send();
    } else { // clip
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let results = JSON.parse(this.response);
                if (results.error != null) {
                    displayError(results.error);
                } else {
                    let r = results.result;
                    document.getElementById("infos-title").innerText = r.title;
                    text = "";
                    if (r.genres) text += "<b>Genres: </b>" + r.genres + "<br/>";
                    if (r.type) text += "<b>Type: </b>" + r.type + "<br/>";
                    if (r.rank) text += "<b>Rank: </b>" + r.rank + "<br/>";
                    if (r.votes) text += "<b>Votes: </b>" + r.votes + "<br/>";
                    if (r.year) text += "<b>Production year: </b>" + r.year + "<br/>";
                    if (r.countries) text += "<b>Production countries: </b>" + r.countries + "<br/>";
                    if (r.directors) text += "<b>Directors: </b>" + r.directors + "<br/>";
                    if (r.producers) text += "<b>Producers: </b>" + r.producers + "<br/>";
                    if (r.writers) text += "<b>Writers: </b>" + r.writers + "<br/>";
                    if (r.actors) text += "<b>Actors: </b>" + r.actors + "<br/>";
                    document.getElementById("modal-text").innerHTML = text;
                }
            }
        };
        xmlhttp.open("GET", "clip_infos.php?id=" + clipId, true);
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlhttp.send();
    }
}

function insertPers() {
    insert({
        table : 'Person',
        name : document.getElementById("p-i-0").value,
        //nickname : document.getElementById("p-i-1").value,
        //birthdate : document.getElementById("p-i-2").value,
        //bio : document.getElementById("p-i-3").value,
    });
}

function insertClip() {
    insert({
        table : 'Clip',
        title : document.getElementById("m-i-1").value,
        year : document.getElementById("m-i-2").value,
    });
}

function insertCountry() {
    insert({
        table : 'Country',
        name : document.getElementById("c-i-0").value,
    });
}

function deletePers() {
    delete_({
        table : 'Person',
        name : document.getElementById("p-d-0").value,
    });
}

function deleteClip() {
    delete_({
        table : 'Clip',
        id : document.getElementById("m-d-0").value,
    });
}

function deleteCountry() {
    delete_({
        table : 'Country',
        name : document.getElementById("c-d-0").value,
    });
}

function runSearch() {
    cleanTable();
    removeError();
    removeMessage();
    toggleLoad(false);
    let query = document.getElementById("search").value;
    
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            toggleLoad(false);
            console.debug(this.response);
            let results = JSON.parse(this.response);
            console.debug(results);
            if (results.error != null) {
                displayError(results.error);
            } else {
                displayMessage("Query took " + results.time + " s.");
                fillTable(results.results.actors, 'results-actors', true, true);
                fillTable(results.results.directors, 'results-directors', true, true);
                fillTable(results.results.producers, 'results-producers', true, true);
                fillTable(results.results.writers, 'results-writers', true, true);
                fillTable(results.results.clips, 'results-clips', true, false);
            }
            document.getElementById('actors-wrap').hidden = false;
            document.getElementById('directors-wrap').hidden = false;
            document.getElementById('producers-wrap').hidden = false;
            document.getElementById('writers-wrap').hidden = false;
            document.getElementById('clips-wrap').hidden = false;
        } else {
            
        }
    };
    xmlhttp.open("GET","search.php?query=" + query);
    xmlhttp.send();
    toggleLoad(true);
}

function printModData() {
    let action = document.getElementById("ActionSelect").value
    let table = document.getElementById("TableSelect").value
    
    if (action === "" || table === "") {return;}
    
    let inserts = [
        document.getElementById("persInsert"),
        document.getElementById("movieInsert"),
        document.getElementById("countryInsert"),
    ];
    let deletes = [
        document.getElementById("persDelete"),
        document.getElementById("movieDelete"),
        document.getElementById("countryDelete"),
    ];
    
    inserts.forEach(function(elem) {
        elem.hidden = true;
    });
    deletes.forEach(function(elem) {
        elem.hidden = true;
    });
    
    if (action === 'INSERT') {
        inserts[table].hidden = false;
    } else {
        deletes[table].hidden = false;
    }
    
}

function openTab(evt, tab) {
    cleanTable();
    removeError();
    removeMessage();
    toggleLoad(false);
    
    document.getElementById("movieTab").style.display = "none";
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tab).style.display = "block";
    evt.currentTarget.className += " active";
}
