<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$error = NULL;
$start = microtime(true);

$ret = [];
$ret['result'] = null;

$sql = "SELECT C.*, (
            SELECT GROUP_CONCAT(g.genre) FROM ClipGenres cg
            LEFT JOIN Genre g ON g.id = cg.genreId
            WHERE cg.clipId = C.id
      ) AS genres, (
            SELECT GROUP_CONCAT(l.language) FROM ClipLanguages cl
            LEFT JOIN Language l ON l.id = cl.languageId
            WHERE cl.clipId = C.id
      ) AS languages, (
            SELECT GROUP_CONCAT(c.country) FROM ClipCountries cc
            LEFT JOIN Country c ON c.id = cc.countryId
            WHERE cc.clipId = C.id
      ) AS countries, (
            SELECT GROUP_CONCAT(p.fullName) FROM ActingRole r
            LEFT JOIN Person p ON p.id = r.personId
            WHERE r.clipId = C.id
      ) AS actors, (
            SELECT GROUP_CONCAT(p.fullName) FROM DirectorRole r
            LEFT JOIN Person p ON p.id = r.personId
            WHERE r.clipId = C.id
      ) AS directors, (
            SELECT GROUP_CONCAT(p.fullName) FROM ProducerRole r
            LEFT JOIN Person p ON p.id = r.personId
            WHERE r.clipId = C.id
      ) AS producers, (
            SELECT GROUP_CONCAT(p.fullName) FROM WriterRole r
            LEFT JOIN Person p ON p.id = r.personId
            WHERE r.clipId = C.id
      ) AS writers
        FROM Clip C
        WHERE C.id = '?'
        GROUP BY C.id
";
try {
    $connection = open_db();
    if ($_SERVER['REQUEST_METHOD'] == "GET") {
        if (array_key_exists('id', $_GET)) {          
            $sql_w_query = str_replace('?', $_GET['id'], $sql);
            $stmt = $connection->prepare($sql_w_query);
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetch();
                if (!$result) {
                    $error = "Unmatching ID";
                } else {
                    $ret['result'] = $result;
                }
            } else {
                $error = $stmt->errorCode();
            }
        } else {
            $error = "Invalid 'query' parameter in GET arguments";
        }
    } else {
        $error = "Request type error";
    }
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

$ret["error"] = $error;
$ret["time"] = microtime(true)-$start;
echo str_replace("''''", "'", json_encode($ret));
