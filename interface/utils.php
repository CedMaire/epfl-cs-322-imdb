<?php

$servername = "pikkle.ch";
$username = "root";
$password = "kittenbay";
$dbname = "imdb";

$unimplemented = "Not implemented yet.";

function open_db() {
    global $servername, $username, $password, $dbname;
    $connection = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $connection;
}

// Outputs results, error and time calculation
function output_json($result, $error, $start) {
    $ret = [];
    $ret["error"] = $error;
    $ret["result"] = $result;
    $ret["time"] = microtime(true)-$start;
    echo str_replace("''''", "'", json_encode($ret));
}
