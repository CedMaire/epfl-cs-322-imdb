from datetime import datetime, timedelta
import sys
import re
import time

DEBUG = True
ERRORS = True

PATH = "output/"

last_debug = time.time()

def clean_string(str):
    return str.replace("\'", "''").replace('\\', '/')


def init_file(filename):
    open(PATH + filename, 'w').close()  # empty file


def dump_to_file(filename, dump):
    init_file(filename)
    with open(PATH + filename, 'a', encoding='utf-8') as file:
        for tup in dump:
            processed = ()
            for t in tup:
                processed += ('\\N',) if t is None else (clean_string(str(t)),)
            file.write(','.join(processed))
            file.write('\n')


def dprint(*args, **kwargs):
    global last_debug
    if DEBUG:
        tmp = time.time()
        print("DEBUG ({}s)\t: {}".format(round(tmp-last_debug, 2), str(*args)), file=sys.stderr, **kwargs)
        last_debug = tmp


def eprint(*args, **kwargs):
    if ERRORS:
        print("ERROR: " + str(*args), file=sys.stderr, **kwargs)


def update_tuple(tup_, map_):
    tupToList = list(tup_)
    for k in map_:
        tupToList[k] = map_[k]
    return tuple(tupToList)


def transform_height(str):
    if str == '':
        return None
    if 'cm' in str:
        return float(str.replace('cm', ' cm').replace('  ', ' ').split(' ')[0])
    else:
        measures = list(filter(lambda n: n != '', str.replace("'", "' ").replace("  ", " ").split(' ')))
        length = 30.48 * int(measures[0].strip("'"))
        if len(measures) >= 2:
            if measures[1] == '1/2"':
                inches = 0.5
            else:
                inches = int(measures[1].strip('"'))
            if len(measures) == 3:
                fraction = measures[2].strip('"').split('/')
                inches += float(fraction[0]) / float(fraction[1])
            length += 2.54 * inches
        return length


######################################################################################################

genres_ids = {}  # {genre -> genre_id}
countries_ids = {}  # {country -> country_id}
languages_ids = {}  # {language -> language_id}
persons_ids = {}  # {fullname -> personId}
clips_data = {}  # {clip_id  -> (Title,Year,Type,Votes,Rank,Genre,Country)}
clips_countries = set()  # [(clip_id, country_id)]
count_persons = 0


def person_id(fullname):
    global count_persons
    if fullname in persons_ids:
        return persons_ids[fullname]
    else:
        pid = count_persons
        persons_ids[fullname] = pid
        count_persons += 1
        return pid


def read_basic_clips():
    with open("../data/clips.csv", "r", encoding='utf-8') as file:
        file.readline()  # skip csv header
        i = 0
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            clip_title = clean_string(row[1].strip())
            clip_year = row[2].strip()
            clip_year = None if clip_year == '' else int(clip_year)
            clip_type = row[3].strip()
            clip_type = None if clip_type == '' else clip_type
            clips_data[clip_id] = (clip_id, clip_title, clip_year, clip_type, None, None)
            i += 1
        dprint("Read {:,} clips".format(i))
        file.close()


def clips():
    items = list(clips_data.values())
    dump_to_file('Clip.csv', items)
    dprint("Inserted clips")


def ratings():
    with open("../data/ratings.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            votes = int(row[1])
            rank = float(row[2])
            clips_data[clip_id] = update_tuple(clips_data[clip_id], {4: votes, 5: rank})
            i += 1
        dprint("Read and connected {:,} rating entries".format(i))


def genres():
    genre_clips = []
    g = 1
    with open("../data/genres.csv", "r", encoding="utf-8") as file:
        file.readline()  # skip csv header
        i = 0
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            genre = row[1].strip()
            if (genre not in genres_ids):
                genres_ids[genre] = g
                g += 1
            genre_clips.append((clip_id, genres_ids[genre]))
            i += 1
        dprint("Read and connected {:,} genre entries".format(i))

    dump_to_file('Genre.csv', list(map(lambda i: (i[1], i[0]), genres_ids.items())))
    dprint("Inserted genres")
    dump_to_file('ClipGenres.csv', genre_clips)
    dprint("Inserted clip-genres")


def countries():
    with open("../data/countries.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        c = 1
        countries_clips = []
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            country = row[1].strip()
            if country not in countries_ids:
                countries_ids[country] = c
                c += 1
            countries_clips.append((clip_id, countries_ids[country]))
            i += 1
        dprint("Read and connected {:,} country entries".format(i))

    dump_to_file('Country.csv', list(map(lambda t: (t[1], t[0]), countries_ids.items())))
    dprint("Inserted countries")
    dump_to_file('ClipCountries.csv', countries_clips)
    dprint("Inserted clip-countries")


def languages():
    data = []  # [(language, clip_id)]
    with open("../data/languages.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        l = 1
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            language = clean_string(row[1].strip())
            if language not in languages_ids:
                languages_ids[language] = l
                l += 1
            data.append((clip_id, languages_ids[language]))
            i += 1
        dprint("Read {:,} language entries".format(i))
    dump_to_file('Language.csv', list(map(lambda t: (t[1], t[0]), languages_ids.items())))
    dprint("Inserted languages")
    dump_to_file('ClipLanguages.csv', data)
    dprint("Insert languages-clips")


def running_times():
    data = []  # [(clip_id,country_id,runningtimes)]
    with open("../data/running_times.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0  # successful rows
        j = 0  # total rows
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            country = row[1].strip()
            try:
                country = countries_ids[country] if country != '' else None
                runs = int(row[2].strip())
                clips_countries.add((clip_id, country))
                data.append((clip_id, country, runs))
                i += 1
            except:
                # either there is no country corresponding, or the runningtime is not a number
                # TODO: report -> notify dropped rows
                pass
            j += 1
        dprint("Read {:,}/{:,} running times entries ({:,} dropped)".format(i, j, j - i))
    dump_to_file('RunningTime.csv', data)
    dprint("Inserted Running Times")


def release_dates():
    data = []  # [(clip_id,country_id,releasedate)]
    with open("../data/release_dates.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        j = 0
        for line in file.readlines():
            row = line.split(',')
            clip_id = row[0]
            country = row[1].strip()
            try:
                country = countries_ids[country] if country != '' else None
                date = datetime.strftime(datetime.strptime(row[2].strip(), '%d %B %Y'), '%Y-%m-%d')
                clips_countries.add((clip_id, country))
                data.append((clip_id, country, date))
                i += 1
            except:
                # either there is no country corresponding, or the date could not be transformed
                # TODO: report -> notify dropped rows
                pass
            j += 1
        dprint("Read {:,}/{:,} release date entries ({:,} dropped)".format(i, j, j - i))
    dump_to_file('ReleaseDate.csv', data)
    dprint("Inserted Running Times")


def clip_country_audience():
    dump_to_file('ClipCountryAudience.csv', list(clips_countries))
    dprint("Inserted Clip Country Audience")


def clip_links():
    data = []  # [(clip_from,clip_to,link_type)]
    with open("../data/clip_links.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            clip_from = row[0]
            clip_to = row[1]
            type = row[2].strip()
            data.append((clip_from, clip_to, type))
            i += 1
        dprint("Read {:,} clip link entries".format(i))

    dump_to_file('ClipLink.csv', data)
    dprint("Inserted Clip Links")


def actors():
    data = []  # [(person_id,clip_id,role,ordercredit,addinfos)]
    with open("../data/actors.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            fname = row[0]
            clips = row[1].strip('[]').split('|')
            roles = row[2].strip('[]').split('|')
            order = row[3].strip('[]').split('|')
            addin = row[4].strip().strip('[]').split('|')
            pid = person_id(fname)
            i += 1
            for j in range(0, len(clips)):
                r = None if roles[j] == '' else clean_string(roles[j])
                o = None if order[j] == '' else clean_string(order[j])
                a = None if addin[j] == '' else clean_string(addin[j])
                data.append((pid, clips[j], r, o, a))
        dprint("Read {:,} actors and {:,} acting roles".format(i, len(data)))
    dump_to_file('ActingRole.csv', data)
    dprint("Inserted Acting roles")


def directors():
    data = []  # [(fullname,clip,role,addinfos)]
    with open("../data/directors.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            fname = row[0]
            clips = row[1].strip('[]').split('|')
            roles = row[2].strip('[]').split('|')
            addin = row[3].strip().strip('[]').split('|')
            pid = person_id(fname)
            i += 1
            for j in range(0, len(clips)):
                r = None if roles[j] == '' else clean_string(roles[j])
                a = None if addin[j] == '' else clean_string(addin[j])
                data.append((pid, clips[j], r, a))
        dprint("Read {:,} directors and {:,} director roles".format(i, len(data)))
    dump_to_file('DirectorRole.csv', data)
    dprint("Inserted Director roles")


def producers():
    data = []  # [(fullname,clip,role,addinfos)]
    with open("../data/producers.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            fname = row[0]
            clips = row[1].strip('[]').split('|')
            roles = row[2].strip('[]').split('|')
            addin = row[3].strip().strip('[]').split('|')
            pid = person_id(fname)
            i += 1
            for j in range(0, len(clips)):
                r = None if roles[j] == '' else clean_string(roles[j])
                a = None if addin[j] == '' else clean_string(addin[j])
                data.append((pid, clips[j], r, a))
        dprint("Successfully read {:,} producers and {:,} producer roles".format(i, len(data)))
    dump_to_file('ProducerRole.csv', data)
    dprint("Inserted Producer roles")


def writers():
    data = []  # [(fullname,clip,role,worktypes,addinfos)]
    with open("../data/writers.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0
        for line in file.readlines():
            row = line.split(',')
            fname = row[0]
            clips = row[1].strip('[]').split('|')
            worktypes = row[2].strip('[]').split('|')
            roles = row[3].strip('[]').split('|')
            addin = row[4].strip().strip('[]').split('|')
            pid = person_id(fname)
            i += 1
            for j in range(0, len(clips)):
                r = None if roles[j] == '' else clean_string(roles[j])
                w = None if worktypes[j] == '' else clean_string(worktypes[j])
                a = None if addin[j] == '' else clean_string(addin[j])
                data.append((pid, clips[j], r, w, a))
        dprint("Successfully read {:,} writers and {:,} writer roles".format(i, len(data)))
    dump_to_file('WriterRole.csv', data)
    dprint("Inserted Writer roles")


def biographies():
    data = []  # [(person_id,RealName,Nickname,DateAndPlaceOfBirth,Height,Biography,Biographer,DateAndCauseOfDeath,Spouse,
    # Trivia,BiographicalBooks,PersonalQuotes,Salary,Trademark,WhereAreTheyNow)]
    spouses_ids = {} # {spouse_name -> spouse_id}
    spouse_persons = []
    bookrows = []
    s = 1 # spouses count
    b = 1 # biobooks count
    with open("../data/biographies.csv", "r", encoding="utf-8") as file:
        file.readline()
        i = 0 # kept rows
        count = 0 # total rows
        for line in file.readlines():
            row = line.split(',')
            count += 1
            try:
                pid = persons_ids[row[0]]
            except:
                continue  # if person is not found, pass the biography
            realname = row[1]
            nickname = row[2]
            birth = row[3]
            try:
                height = transform_height(row[4])
            except:
                height = None
            biography = row[5]
            biographer = row[6]
            death = row[7]
            spouse = row[8].strip('[]').split('|')
            trivia = row[9]
            books = row[10].strip('[]').split('|')
            quotes = row[11]
            salary = None
            if row[12] != '':
                salary = 0
                salaries = row[12].strip('[]').split('|')
                for salary_ in salaries:
                    if len(salary_.split('->')) > 2:
                        salaryparsed = re.sub("\D", "", salary_.split('->')[1].split(' ')[0])
                        if salaryparsed != '':
                            salary += int(salaryparsed)
            trademark = row[13]
            andnow = row[14].strip()

            for j in range(0, len(spouse)):
                if spouse[j] != '':
                    spouse_name = spouse[j][1:].split("'")[0]
                    if spouse_name not in spouses_ids:
                        spouses_ids[spouse_name] = s
                        s += 1
                    spouse_persons.append((spouses_ids[spouse_name], pid))

            for j in range(0, len(books)):
                if books[j] != '':
                    bookrows.append((b, clean_string(books[j]), pid))
                    b += 1

            data.append((pid, clean_string(realname), clean_string(nickname), clean_string(birth), height,
                         clean_string(biography), clean_string(biographer), clean_string(death), clean_string(trivia),
                         clean_string(quotes), salary, clean_string(trademark), clean_string(andnow)))
            i += 1
        dprint("Successfully read {:,}/{:,} biographies ({:,} dropped)".format(i, count, count-i))

    dump_to_file('Spouse.csv', list(map(lambda t: (t[1], t[0]), spouses_ids.items())))
    dprint("Inserted spouses")
    dump_to_file('SpousePerson.csv', spouse_persons)
    dprint("Inserted spouses relations")
    dump_to_file('Biography.csv', data)
    dprint("Inserted biographies")
    dump_to_file('BioBook.csv', bookrows)
    dprint("Inserted books")


def persons():
    dump_to_file('Person.csv', list(map(lambda t: (t[1], t[0]), persons_ids.items())))
    dprint("Inserted Persons")


# calls to all file parsers
# read_basic_clips()
# ratings()
genres()
# countries()
# clips()
# languages()
# running_times()
# release_dates()
# clip_country_audience()
# clip_links()
# actors()
# directors()
# producers()
# writers()
# biographies()
# persons()
