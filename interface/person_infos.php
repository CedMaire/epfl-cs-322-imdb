<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$error = NULL;
$start = microtime(true);

$ret = [];
$ret['result'] = null;

$sql = "SELECT P.id, P.fullName, B.realName, B.nickname, B.datePlaceBirth, B.dateCauseDeath,
    B.height, B.biography, B.biographer, B.trivia, B.personalQuotes, B.salary, B.trademark,
    B.whereAreTheyNow,
    (
    SELECT GROUP_CONCAT(c.title) FROM ActingRole r
    LEFT JOIN Clip c ON c.id = r.clipId
    WHERE r.personId = P.id
    ) AS actings,
    (
    SELECT GROUP_CONCAT(c.title) FROM DirectorRole r
    LEFT JOIN Clip c ON c.id = r.clipId
    WHERE r.personId = P.id
    ) AS directings,
    (
    SELECT GROUP_CONCAT(c.title) FROM ProducerRole r
    LEFT JOIN Clip c ON c.id = r.clipId
    WHERE r.personId = P.id
    ) AS producings,
    (
    SELECT GROUP_CONCAT(c.title) FROM WriterRole r
    LEFT JOIN Clip c ON c.id = r.clipId
    WHERE r.personId = P.id
    ) AS writings,
    (
    SELECT GROUP_CONCAT(s.name) FROM SpousePerson r
    LEFT JOIN Spouse s ON s.id = r.spouseId
    WHERE r.personId = P.id
    ) AS spouses,
    (
    SELECT GROUP_CONCAT(bb.title) FROM BioBook bb
    WHERE bb.personId = P.id
    ) AS biobooks
    FROM Person P
    LEFT JOIN Biography B on P.id = B.personId
    WHERE P.id = '?'
    GROUP BY P.id
";
try {
    $connection = open_db();
    if ($_SERVER['REQUEST_METHOD'] == "GET") {
        if (array_key_exists('id', $_GET)) {          
            $sql_w_query = str_replace('?', $_GET['id'], $sql);
            $stmt = $connection->prepare($sql_w_query);
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $result = $stmt->fetch();
                if (!$result) {
                    $error = "Unmatching ID";
                } else {
                    $ret['result'] = $result;
                }
            } else {
                $error = $stmt->errorCode();
            }
        } else {
            $error = "Invalid 'query' parameter in GET arguments";
        }
    } else {
        $error = "Request type error";
    }
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

$ret["error"] = $error;
$ret["time"] = microtime(true)-$start;
echo str_replace("''''", "'", json_encode($ret));
