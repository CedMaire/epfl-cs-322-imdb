DROP SCHEMA IF EXISTS imdb;
CREATE SCHEMA imdb;
USE imdb;

CREATE TABLE Spouse (
  id   INT PRIMARY KEY,
  name VARCHAR(255) CHARACTER SET utf8 UNIQUE NOT NULL
);


CREATE TABLE Person (
  id       INT PRIMARY KEY,
  fullName VARCHAR(255) CHARACTER SET utf8 UNIQUE NOT NULL
);

CREATE TABLE SpousePerson (
  spouseId INT NOT NULL,
  personId INT NOT NULL,
  CONSTRAINT Wedding_spouse_fk
  FOREIGN KEY (spouseId) REFERENCES Spouse (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT Wedding_person_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE Biography (
  personId        INT PRIMARY KEY,
  realName        VARCHAR(255) CHARACTER SET utf8 NULL,
  nickname        VARCHAR(255) CHARACTER SET utf8 NULL,
  datePlaceBirth  TEXT CHARACTER SET utf8         NULL,
  height          INT                             NULL,
  biography       TEXT CHARACTER SET utf8         NULL,
  biographer      VARCHAR(255) CHARACTER SET utf8 NULL,
  dateCauseDeath  TEXT CHARACTER SET utf8         NULL,
  trivia          TEXT CHARACTER SET utf8         NULL,
  personalQuotes  TEXT CHARACTER SET utf8         NULL,
  salary          VARCHAR(255) CHARACTER SET utf8 NULL,
  trademark       TEXT CHARACTER SET utf8         NULL,
  whereAreTheyNow TEXT CHARACTER SET utf8         NULL,
  CONSTRAINT Biography_Person_person_id_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE BioBook (
  id       INT PRIMARY KEY,
  title    TEXT CHARACTER SET utf8 NOT NULL,
  personId INT                     NOT NULL,
  CONSTRAINT Biobook_biographies_fk
  FOREIGN KEY (personId) REFERENCES Biography (personId)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE Country (
  id      INT PRIMARY KEY,
  country VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Genre (
  id    INT PRIMARY KEY,
  genre VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Language (
  id       INT PRIMARY KEY,
  language VARCHAR(255) CHARACTER SET utf8 NOT NULL
);

CREATE TABLE Clip (
  id    INT PRIMARY KEY,
  title VARCHAR(255) CHARACTER SET utf8 NOT NULL,
  year  INT(4),
  type  ENUM ('V', 'VG', 'TV', 'SE'),
  votes INT,
  rank  FLOAT
);

CREATE TABLE ClipGenres (
  clipId  INT NOT NULL,
  genreId INT NOT NULL,
  CONSTRAINT ClipGenre_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ClipGenre_Genre_id_fk
  FOREIGN KEY (genreId) REFERENCES Genre (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ClipCountries (
  clipId    INT NOT NULL,
  countryId INT NOT NULL,
  CONSTRAINT ClipCountry_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ClipCountry_Country_id_fk
  FOREIGN KEY (countryId) REFERENCES Country (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ClipLanguages (
  clipId     INT NOT NULL,
  languageId INT NOT NULL,
  CONSTRAINT ClipsLanguages_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ClipsLanguages_Language_id_fk
  FOREIGN KEY (languageId) REFERENCES Language (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ClipLink (
  fromId INT   NOT NULL,
  toId   INT   NOT NULL,
  type   ENUM  ('follows',
                'followed by',
                'references',
                'referenced in',
                'features',
                'version of',
                'edited into',
                'edited from',
                'spoofed in',
                'featured in',
                'remake of',
                'spoofs',
                'spin off',
                'remade as',
                'alternate language version of') NOT NULL,
  CONSTRAINT ClipLink_Clip_from_id_fk
  FOREIGN KEY (fromId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ClipLink_Clip_to_id_fk
  FOREIGN KEY (toId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ClipCountryAudience (
  clipId    INT NOT NULL,
  countryId INT,
  UNIQUE (clipId, countryId),
  CONSTRAINT ClipCountryAudience_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ClipCountryAudience_Country_country_fk
  FOREIGN KEY (countryId) REFERENCES Country (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ReleaseDate (
  clipId    INT  NOT NULL,
  countryId INT,
  date      DATE NOT NULL,
  CONSTRAINT ClipCountryAudience_ReleaseDate_fk
  FOREIGN KEY (clipId, countryId) REFERENCES ClipCountryAudience (clipId, countryId)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE RunningTime (
  clipId      INT NOT NULL,
  countryId   INT,
  runningTime INT NOT NULL,
  CONSTRAINT ClipCountryAudience_RunningTime_fk
  FOREIGN KEY (clipId, countryId) REFERENCES ClipCountryAudience (clipId, countryId)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ActingRole (
  personId    INT                             NOT NULL,
  clipId      INT                             NOT NULL,
  roleName    VARCHAR(255) CHARACTER SET utf8 NULL,
  creditOrder INT                             NULL,
  addInfo     TEXT CHARACTER SET utf8         NULL,
  CONSTRAINT ActingRole_personId_clipId_roleName_pk
  UNIQUE (personId, clipId, roleName, creditOrder, addInfo(200)),
  CONSTRAINT ActingRole_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ActingRole_Person_id_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE DirectorRole (
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL,
  CONSTRAINT DirectorRole_personId_clipId_roleName_pk
  UNIQUE (personId, clipId, roleName, addInfo(200)),
  CONSTRAINT DirectorRole_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT DirectorRole_Person_id_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE ProducerRole (
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL,
  CONSTRAINT ProducerRole_personId_clipId_roleName_pk
  UNIQUE (personId, clipId, roleName, addInfo(200)),
  CONSTRAINT ProducerRole_Person_id_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT ProducerRole_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);

CREATE TABLE WriterRole
(
  personId INT                             NOT NULL,
  clipId   INT                             NOT NULL,
  roleName VARCHAR(255) CHARACTER SET utf8 NULL,
  workType VARCHAR(255) CHARACTER SET utf8 NULL,
  addInfo  TEXT CHARACTER SET utf8         NULL,
  CONSTRAINT WriterRole_personId_clipId_roleName_pk
  UNIQUE (personId, clipId, roleName, addInfo(200)),
  CONSTRAINT WriterRole_Person_id_fk
  FOREIGN KEY (personId) REFERENCES Person (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  CONSTRAINT WriterRole_Clip_id_fk
  FOREIGN KEY (clipId) REFERENCES Clip (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
);
