<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$error = NULL;
$start = microtime(true);

$ret = [];

function newId($connection, $tablename) {
    $biggest_id_sql = "SELECT MAX(id) FROM " . $tablename;
    $stmt_id = $connection->prepare($biggest_id_sql);
    $stmt_id->execute();
    return intval($stmt_id->fetch()[0]) + 1; // return MAX + 1
}

try {
    $connection = open_db();
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if (array_key_exists('table', $_POST) && in_array($_POST['table'], ["Clip", "Person", "Country"])) {
            $id = newId($connection, $_POST['table']);
            switch($_POST['table']) {
                case "Clip":
                $sql = "INSERT INTO Clip (id, title, year) VALUES (:id, :title, :year)";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":id", $id);
                $stmt->bindParam(":title", $_POST['title']);
                $stmt->bindParam(":year", $_POST['year']);
                break;
                
                case "Person":
                $sql = "INSERT INTO Person (id, fullName) VALUES (:id, :name)";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":id", $id);
                $stmt->bindParam(":name", $_POST['name']);
                break;
                
                case "Country":
                $sql = "INSERT INTO Country (id, country) VALUES (:id, :name)";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":id", $id);
                $stmt->bindParam(":name", $_POST['name']);
                break;
                
                default: return;
            }
            
            // Accessing a maybe-not-defined var is bad, because PHP itself is bad; but who cares ?
            if ($stmt->execute()) {
                $ret['id'] = $id;
            } else {
                $error = $stmt->errorCode();
            }
        } else {
            $error = "Invalid 'table' parameter in POST arguments";
        }
    } else {
        $error = "Request type error";
    }
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

$ret["error"] = $error;
$ret["time"] = microtime(true)-$start;
echo str_replace("''''", "'", json_encode($ret));
