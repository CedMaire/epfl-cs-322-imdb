# mysqldump --xml --no-data -h pikkle.ch -u root -pkittenbay imdb > /Users/mairecedric/Downloads/imdb_struct.xml
# https://www.eversql.com/exporting-mysql-schema-structure-to-xml-using-mysql-clients/
# https://www.eversql.com/sql-query-optimizer/
# https://stackoverflow.com/questions/2955459/what-is-an-index-in-sql

ALTER TABLE `ActingRole` ADD INDEX `actingrole_idx_personid_clipid` (`personId`,`clipId`);
ALTER TABLE `ActingRole` ADD INDEX `actingrole_idx_personid_creditorder` (`personId`,`creditOrder`);
ALTER TABLE `ActingRole` ADD INDEX `actingrole_idx_personid` (`personId`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_year_rank` (`year`,`rank`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_year_rankDesc` (`year`,`rank` DESC);
ALTER TABLE `Clip` ADD INDEX `clip_idx_id_rank` (`id`,`rank`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_type` (`type`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_year` (`year`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_id` (`id`);
ALTER TABLE `Clip` ADD INDEX `clip_idx_rank` (`rank` DESC);
ALTER TABLE `Clip` ADD INDEX `clip_idx_votes` (`votes` DESC);
ALTER TABLE `ClipCountries` ADD INDEX `clipcountries_idx_clipId_countryId` (`clipId`, `countryId`);
ALTER TABLE `ClipCountryAudience` ADD INDEX `clipcountryaudience_idx_clipId_countryId` (`clipId`, `countryId`);
ALTER TABLE `ClipGenres` ADD INDEX `clipgenres_idx_id_clipid_genreid` (`clipId`, `genreId`);
ALTER TABLE `ClipLanguages` ADD INDEX `cliplanguages_idx_id_clipid_languageid` (`clipId`, `languageId`);
ALTER TABLE `Country` ADD INDEX `country_idx_id_country` (`id`, `country`);
ALTER TABLE `DirectorRole` ADD INDEX `directorrole_idx_personid_clipid` (`personId`,`clipId`);
ALTER TABLE `DirectorRole` ADD INDEX `directorrole_idx_personid` (`personId`);
ALTER TABLE `Language` ADD INDEX `language_idx_id_id_language` (`id`, `language`);
ALTER TABLE `ProducerRole` ADD INDEX `producerrole_idx_personid_clipid` (`personId`,`clipId`);
ALTER TABLE `Person` ADD INDEX `person_idx_id_fullname` (`id`,`fullName`);
ALTER TABLE `Person` ADD INDEX `person_idx_fullname` (`fullName`);
ALTER TABLE `Person` ADD INDEX `person_idx_id` (`id`);
ALTER TABLE `ReleaseDate` ADD INDEX `releasedate_idx_date` (`date` DESC);
ALTER TABLE `ReleaseDate` ADD INDEX `releasedate_idx_clipid_countryid` (`clipId`, `countryId`);
ALTER TABLE `RunningTime` ADD INDEX `runningtime_idx_runningtime` (`runningTime` DESC);
ALTER TABLE `RunningTime` ADD INDEX `runningtime_idx_clipid_countryid` (`clipId`, `countryId`);
ALTER TABLE `SpousePerson` ADD INDEX `spouseperson_idx_personid` (`personId`);
ALTER TABLE `WriterRole` ADD INDEX `writerrole_idx_personid_clipid` (`personId`,`clipId`);

# =================================================================================================
# Queries 1
# =================================================================================================

# a) Print the name and length of the 10 longest clips that were released in France.
SELECT DISTINCT Cl.title, RT.runningTime
FROM Clip Cl, RunningTime RT
WHERE Cl.id = RT.clipId AND RT.countryId = (SELECT DISTINCT Co.id
                                            FROM Country Co
                                            WHERE Co.country = 'France')
ORDER BY RT.runningTime DESC
LIMIT 10;

# b) Compute the number of clips released per country in 2001.
SELECT DISTINCT Co.country, ClipsPerCountryId.count
FROM Country Co, (SELECT DISTINCT RD.countryId, COUNT(DISTINCT C.id) AS count
                  FROM Clip C, ReleaseDate RD
                  WHERE C.id = RD.clipId AND YEAR(RD.date) = 2001
                  GROUP BY RD.countryId
                  ORDER BY NULL) ClipsPerCountryId
WHERE Co.id = ClipsPerCountryId.countryId;

# c) Compute the numbers of clips per genre released in the USA after 2013.
SELECT DISTINCT G.genre, ClipsPerGenre.count
FROM Genre G, (SELECT DISTINCT CG.genreId, COUNT(DISTINCT C.id) AS count
               FROM Clip C, ReleaseDate RD, ClipGenres CG
               WHERE C.id = RD.clipId
                     AND C.id = CG.clipId
                     AND RD.countryId = (SELECT DISTINCT Co.id
                                         FROM Country Co
                                         WHERE Co.country = 'USA')
                     AND YEAR(RD.date) >= 2013
               GROUP BY CG.genreId
               ORDER BY NULL) ClipsPerGenre
WHERE G.id = ClipsPerGenre.genreId;

# d) Print the name of actor/actress who has acted in more clips than anyone else.
SELECT DISTINCT P.fullName, TMP1.maxClips
FROM Person P
INNER JOIN (SELECT DISTINCT TMP.personId, MAX(TMP.numClips) AS maxClips
            FROM (SELECT DISTINCT AR.personId, COUNT(DISTINCT AR.clipId) AS numClips
                  FROM ActingRole AR
                  GROUP BY AR.personId
                  ORDER BY NULL) TMP) AS TMP1
ON P.id = TMP1.personId;

# e) Print the maximum number of clips any director has directed.
SELECT DISTINCT MAX(TMP.numClips) AS max
FROM (SELECT DISTINCT COUNT(DISTINCT DR.clipId) AS numClips
      FROM DirectorRole DR
      GROUP BY DR.personId
      ORDER BY NULL) TMP;

/*
f) Print the names of people that had at least 2 different jobs in a single clip. For example, if X has both acted,
    directed and written movie Y, his/her name should be printed out. On the other hand, if X has acted as 4 different
    personas in the same clip, but done nothing else, he/she should not be printed.
*/
# TODO: WORKS, BUT TAKES WAY TOO LONG
SELECT DISTINCT P.fullName
FROM Person P
WHERE EXISTS (SELECT DISTINCT 1
              FROM (SELECT DISTINCT AR.personId, AR.clipId
                    FROM ActingRole AR
                      UNION ALL
                    SELECT DISTINCT DR.personId, DR.clipId
                    FROM DirectorRole DR
                      UNION ALL
                    SELECT DISTINCT PR.personId, PR.clipId
                    FROM ProducerRole PR
                      UNION ALL
                    SELECT DISTINCT WR.personId, WR.clipId
                    FROM WriterRole WR) TMP
              WHERE TMP.personId = P.id
              GROUP BY TMP.clipId
              HAVING COUNT(*) >= 2
              ORDER BY NULL);

# g) Print the 10 most common clip languages.
SELECT DISTINCT L.language
FROM Language L
INNER JOIN (SELECT DISTINCT CL.languageId
            FROM ClipLanguages CL
            GROUP BY CL.languageId
            ORDER BY COUNT(DISTINCT CL.clipId) DESC
            LIMIT 10) IDs
ON L.id = IDs.languageId;

# h) Print the full name of the actor who has performed in the highest number of
# CLIPS WITH A USER-SPECIFIED TYPE
SELECT DISTINCT P.fullName
FROM Person P
WHERE P.id IN (SELECT DISTINCT TMP.personId
               FROM (SELECT DISTINCT TMP1.personId, MAX(TMP1.maxClips)
                     FROM (SELECT DISTINCT AR.personId, COUNT(DISTINCT C.id) AS maxClips
                           FROM ActingRole AR, (SELECT DISTINCT C1.id
                                                FROM Clip C1
                                                WHERE C1.type = 'VG') C
                           WHERE AR.clipId = C.id
                           GROUP BY AR.personId
                           ORDER BY NULL) TMP1) TMP);

# =================================================================================================
# Insert / Delete
# =================================================================================================

#INSERT INTO Clip (id, title, year, type, votes, rank, genre, country)
#VALUES ('?', '?', '?', '?', '?', '?', '?', '?');

#INSERT INTO Person (id, fullName)
#VALUES ((SELECT MAX(id) FROM Person) + 1, '?');


# =================================================================================================
# Simpler Search Queries
# =================================================================================================

SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM ActingRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE);

SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM ProducerRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE);

SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM DirectorRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE);

SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM WriterRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE);

SELECT DISTINCT * FROM Clip WHERE MATCH (title) AGAINST ('?' IN BOOLEAN MODE)

# =================================================================================================
# Search Queries
# =================================================================================================

# Basic Search Functionality
# Search In Actors
SELECT P.id, P.fullName, B.biography FROM Person P
      LEFT JOIN ActingRole AR ON P.id = AR.personId
      LEFT JOIN Biography B ON P.id = B.personId
      WHERE (LOCATE('?', P.fullName) > 0
            OR LOCATE('?', AR.roleName) > 0
            OR LOCATE('?', AR.creditOrder) > 0
            OR LOCATE('?', AR.addInfo) > 0
            OR LOCATE('?', B.realName) > 0
            OR LOCATE('?', B.nickname) > 0
            OR LOCATE('?', B.datePlaceBirth) > 0
            OR B.height = '?'
            OR LOCATE('?', B.biography) > 0
            OR LOCATE('?', B.biographer) > 0
            OR LOCATE('?', B.dateCauseDeath) > 0
            OR LOCATE('?', B.trivia) > 0
            OR LOCATE('?', B.personalQuotes) > 0
            OR LOCATE('?', B.salary) > 0
            OR LOCATE('?', B.trademark) > 0
            OR LOCATE('?', B.whereAreTheyNow) > 0);

SELECT DISTINCT *
FROM Biography B, ActingRole AR, SpousePerson SP
WHERE B.personId = AR.personId AND
      B.personId = SP.personId AND
      (LOCATE('?', AR.roleName) > 0
        OR LOCATE('?', AR.creditOrder) > 0
        OR LOCATE('?', AR.addInfo) > 0
        OR LOCATE('?', B.realName) > 0
        OR LOCATE('?', B.nickname) > 0
        OR LOCATE('?', B.datePlaceBirth) > 0
        OR B.height = '?'
        OR LOCATE('?', B.biography) > 0
        OR LOCATE('?', B.biographer) > 0
        OR LOCATE('?', B.dateCauseDeath) > 0
        OR LOCATE('?', B.trivia) > 0
        OR LOCATE('?', B.personalQuotes) > 0
        OR LOCATE('?', B.salary) > 0
        OR LOCATE('?', B.trademark) > 0
        OR LOCATE('?', B.whereAreTheyNow) > 0);

# Search In Directors
SELECT DISTINCT *
FROM Biography B, DirectorRole DR, SpousePerson SP
WHERE B.personId = DR.personId AND
      B.personId = SP.personId AND
      (LOCATE('?', DR.roleName) > 0
        OR LOCATE('?', DR.addInfo) > 0
        OR LOCATE('?', B.realName) > 0
        OR LOCATE('?', B.nickname) > 0
        OR LOCATE('?', B.datePlaceBirth) > 0
        OR B.height = '?'
        OR LOCATE('?', B.biography) > 0
        OR LOCATE('?', B.biographer) > 0
        OR LOCATE('?', B.dateCauseDeath) > 0
        OR LOCATE('?', B.trivia) > 0
        OR LOCATE('?', B.personalQuotes) > 0
        OR LOCATE('?', B.salary) > 0
        OR LOCATE('?', B.trademark) > 0
        OR LOCATE('?', B.whereAreTheyNow) > 0);

# Search In Producer
SELECT DISTINCT *
FROM Biography B, ProducerRole PR, SpousePerson SP
WHERE B.personId = PR.personId AND
      B.personId = SP.personId AND
      (LOCATE('?', PR.roleName) > 0
        OR LOCATE('?', PR.addInfo) > 0
        OR LOCATE('?', B.realName) > 0
        OR LOCATE('?', B.nickname) > 0
        OR LOCATE('?', B.datePlaceBirth) > 0
        OR B.height = '?'
        OR LOCATE('?', B.biography) > 0
        OR LOCATE('?', B.biographer) > 0
        OR LOCATE('?', B.dateCauseDeath) > 0
        OR LOCATE('?', B.trivia) > 0
        OR LOCATE('?', B.personalQuotes) > 0
        OR LOCATE('?', B.salary) > 0
        OR LOCATE('?', B.trademark) > 0
        OR LOCATE('?', B.whereAreTheyNow) > 0);

# Search In Writer
SELECT DISTINCT *
FROM Biography B, WriterRole WR, SpousePerson SP
WHERE B.personId = WR.personId AND
      B.personId = SP.personId AND
      (LOCATE('?', WR.roleName) > 0
        OR LOCATE('?', WR.workType) > 0
        OR LOCATE('?', WR.addInfo) > 0
        OR LOCATE('?', B.realName) > 0
        OR LOCATE('?', B.nickname) > 0
        OR LOCATE('?', B.datePlaceBirth) > 0
        OR B.height = '?'
        OR LOCATE('?', B.biography) > 0
        OR LOCATE('?', B.biographer) > 0
        OR LOCATE('?', B.dateCauseDeath) > 0
        OR LOCATE('?', B.trivia) > 0
        OR LOCATE('?', B.personalQuotes) > 0
        OR LOCATE('?', B.salary) > 0
        OR LOCATE('?', B.trademark) > 0
        OR LOCATE('?', B.whereAreTheyNow) > 0);

# Search In Clips
SELECT DISTINCT *
FROM Clip C
WHERE (LOCATE('?', C.title) > 0
      OR C.year = '?'
      OR C.type = '?'
      OR C.votes = '?'
      OR C.rank = '?'
      OR C.id IN (SELECT DISTINCT CG.clipId
                  FROM ClipGenres CG
                  WHERE CG.genreId = (SELECT DISTINCT G.id
                                      FROM Genre G
                                      WHERE G.genre = '?'))
      OR C.id IN (SELECT DISTINCT CC.clipId
                  FROM ClipCountries CC
                  WHERE CC.countryId = (SELECT DISTINCT C.id
                                     FROM Country C
                                     WHERE C.country = '?'))
      OR C.id IN (SELECT DISTINCT CL.clipId
                  FROM ClipLanguages CL
                  WHERE CL.languageId = (SELECT DISTINCT L.id
                                         FROM Language L
                                         WHERE L.language = '?'))
      OR C.id IN (SELECT DISTINCT RD.clipId
                  FROM ReleaseDate RD
                  WHERE RD.date = '?')
      OR C.id IN (SELECT DISTINCT RT.clipId
                  FROM RunningTime RT
                  WHERE RT.runningTime = '?'));

# =================================================================================================
# Queries 2
# =================================================================================================

# a) Print the names of the top 10 actors ranked by the average rating of their 3 highest-rated clips that
# where voted by at least 100 people. The actors must have had a role in at least 5 clips (not necessarily
# rated).
# TODO: WORKS, BUT TAKES WAY TOO LONG
CREATE TEMPORARY TABLE MinVotesClips AS (SELECT DISTINCT C.id
                                         FROM Clip C
                                         WHERE C.votes >= 100);

CREATE TEMPORARY TABLE MinClipsActors AS (SELECT DISTINCT AR.personId
                                          FROM ActingRole AR
                                          GROUP BY AR.personId
                                          HAVING COUNT(DISTINCT AR.clipId) >= 5
                                          ORDER BY NULL);

CREATE TEMPORARY TABLE EligibleActingRole AS (SELECT DISTINCT AR.personId, AR.clipId
                                              FROM ActingRole AR
                                              WHERE AR.personId IN (SELECT DISTINCT MCA.personId
                                                                    FROM MinClipsActors MCA)
                                                    AND AR.clipId IN (SELECT DISTINCT MVC.id
                                                                      FROM MinVotesClips MVC));

CREATE TEMPORARY TABLE EligibleActingRoleRank AS (SELECT DISTINCT EAR.personId, EAR.clipId, C.rank
                                                  FROM EligibleActingRole EAR
                                                  INNER JOIN Clip C
                                                  ON EAR.clipId = C.id);

CREATE TEMPORARY TABLE HighestRatedActingRole AS (SELECT RankedClipsEligibleActingRole.personId, RankedClipsEligibleActingRole.clipId, RankedClipsEligibleActingRole.rank
                                                  FROM (SELECT EARR.personId, EARR.clipId, EARR.rank,
                                                          @CLIP_RANK := if(@CURRENT_PERSON = EARR.personId, @CLIP_RANK + 1, 1) AS clipRank,
                                                          @CURRENT_PERSON := EARR.personId AS currentPerson
                                                        FROM EligibleActingRoleRank EARR
                                                        ORDER BY EARR.personId, EARR.rank DESC) RankedClipsEligibleActingRole
                                                  WHERE RankedClipsEligibleActingRole.clipRank <= 3);

SELECT P.fullName
FROM Person P
INNER JOIN (SELECT DISTINCT HRAR.personId, AVG(HRAR.rank) AS average
            FROM HighestRatedActingRole HRAR
            GROUP BY HRAR.personId
            ORDER BY average DESC
            LIMIT 10) TOP10
ON P.id = TOP10.personId;

# b) Compute the average rating of the top-100 rated clips per decade in decreasing order.
# We suppose that the average should be in decreasing order (it could have been the decade).
SELECT AVG(RankedClips.rank) AS average, RankedClips.currentDecade * 10 AS decade
FROM (SELECT C.year, C.rank,
        @CLIP_RANK := if(@CURRENT_DECADE = FLOOR(C.year / 10), @CLIP_RANK + 1, 1) AS clipRank,
        @CURRENT_DECADE := FLOOR(C.year / 10) AS currentDecade
      FROM Clip C
      WHERE C.year IS NOT NULL AND C.rank IS NOT NULL
      ORDER BY C.year, C.rank DESC) RankedClips
WHERE RankedClips.clipRank <= 100
GROUP BY decade
ORDER BY average DESC;

# c) For any video game director, print the first year he/she directed a game, his/her name and all his/her
# game titles from that year.
SELECT DISTINCT P.fullName, C.year, C.title
FROM Person P, DirectorRole DR1, Clip C
WHERE P.id = DR1.personId
      AND DR1.clipId = C.id
      AND C.year = (SELECT DISTINCT C.year
                    FROM DirectorRole DR, Clip C
                    WHERE P.id = DR.personId AND DR.clipId = C.id AND C.type = 'VG'
                    ORDER BY C.year
                    LIMIT 1);

# d) For each year, print the title, year and rank-in-year of top 3 clips, based on their ranking.
# http://www.sqlines.com/mysql/how-to/get_top_n_each_group
SELECT RankedClips.year, RankedClips.title, RankedClips.clipRank AS rankInYear
FROM (SELECT C.year, C.title, C.rank,
        @CLIP_RANK := if(@CURRENT_YEAR = C.year, @CLIP_RANK + 1, 1) AS clipRank,
        @CURRENT_YEAR := C.year AS currentYear
      FROM Clip C
      WHERE C.year IS NOT NULL AND C.rank IS NOT NULL
      ORDER BY C.year, C.rank DESC) RankedClips
WHERE RankedClips.clipRank <= 3;

# e) Print the names of all directors who have also written scripts for clips, in all of which they were
# additionally actors (but not necessarily directors) and every clip they directed has at least two more
# points in ranking than any clip they wrote.
CREATE TEMPORARY TABLE EligibleDirectorRole AS (SELECT DISTINCT DR.personId
                                                FROM DirectorRole DR, Clip C1
                                                WHERE DR.clipId = C1.id
                                                GROUP BY DR.personId
                                                HAVING MIN(C1.rank) >= 2 + (SELECT DISTINCT MAX(C.rank)
                                                                            FROM WriterRole WR, Clip C
                                                                            WHERE DR.personId = WR.personId AND WR.clipId = C.id)
                                                ORDER BY NULL);

SELECT DISTINCT P.fullName
FROM Person P
INNER JOIN (SELECT DISTINCT EDR.personId, COUNT(DISTINCT WR.clipId) AS totalWritingClips
            FROM EligibleDirectorRole EDR, WriterRole WR
            WHERE EDR.personId = WR.personId
            GROUP BY EDR.personId
            HAVING totalWritingClips = (SELECT DISTINCT COUNT(DISTINCT AR1.clipId)
                                        FROM ActingRole AR1
                                        WHERE EDR.personId = AR1.personId)
                   AND totalWritingClips = (SELECT DISTINCT COUNT(DISTINCT WR1.clipId)
                                            FROM WriterRole WR1
                                            WHERE EDR.personId = WR1.personId)
            ORDER BY NULL) IDCount
ON P.id = IDCount.personId;

# f) Print the names of the actors that are not married and have participated in more than 2 clips that they
# both acted in and co-directed it.
# TODO: WORKS, BUT TAKES WAY TOO LONG
CREATE TEMPORARY TABLE 2ClipsActedDirected AS (SELECT DISTINCT TMP.personId
                                               FROM (SELECT DISTINCT AR.personId, AR.clipId
                                                     FROM (SELECT DISTINCT AR1.personId, AR1.clipId
                                                           FROM ActingRole AR1) AR
                                                     INNER JOIN (SELECT DISTINCT DR1.personId, DR1.clipId
                                                                 FROM DirectorRole DR1) DR
                                                     ON AR.personId = DR.personId AND AR.clipId = DR.clipId) TMP
                                               GROUP BY TMP.personId
                                               HAVING COUNT(DISTINCT TMP.clipId) >= 3);

SELECT DISTINCT P.fullName
FROM Person P
INNER JOIN 2ClipsActedDirected 2CAD
ON P.id = 2CAD.personId AND P.id NOT IN (SELECT DISTINCT SP.personId
                                         FROM SpousePerson SP);

# g) Print the names of screenplay story writers who have worked with more than 2 producers.
CREATE TEMPORARY TABLE SPSWriterRoles AS (SELECT DISTINCT *
                                          FROM WriterRole WR
                                          WHERE WR.workType = 'screenplay');

SELECT DISTINCT P.fullName
FROM Person P, SPSWriterRoles SPSWR, ProducerRole PR
WHERE P.id = SPSWR.personId AND SPSWR.clipId = PR.clipId
GROUP BY P.id
HAVING COUNT(DISTINCT PR.personId) >= 3
ORDER BY NULL;

# h) Compute the average rating of an actor's clips (for each actor) when she/he has a leading role (first 3
# credits in the clip).
SELECT DISTINCT P.fullName, AVG(C.rank) AS average
FROM Person P
INNER JOIN (SELECT DISTINCT AR1.personId, AR1.clipId, AR1.creditOrder
            FROM ActingRole AR1) AR
ON P.id = AR.personId
INNER JOIN (SELECT DISTINCT C1.id, C1.rank
            FROM Clip C1
            WHERE C1.rank IS NOT NULL) C
ON AR.clipId = C.id
WHERE AR.creditOrder <= 3
GROUP BY P.id
ORDER BY NULL;

# i) Compute the average rating for the clips whose genre is the most popular genre.
SELECT DISTINCT AVG(C.rank) AS average
FROM Clip C
INNER JOIN (SELECT DISTINCT CG1.clipId
            FROM ClipGenres CG1
            INNER JOIN (SELECT DISTINCT MPG.genreId, MAX(MPG.amount)
                        FROM (SELECT DISTINCT CG.genreId, COUNT(*) AS amount
                              FROM ClipGenres CG
                              GROUP BY CG.genreId
                              ORDER BY NULL) AS MPG
                        WHERE MPG.genreId IS NOT NULL) AS TMP
            ON CG1.genreId = TMP.genreId) TMP
ON C.id = TMP.clipId
WHERE C.rank IS NOT NULL;

# j) Print the names of the actors that have participated in more than 100 clips, of which at least 60% where
# short but not comedies nor dramas, and have played in more comedies than double the dramas. Print
# also the number of comedies and dramas each of them participated in.
# TODO: WORKS, BUT TAKES WAY TOO LONG
SET @COMEDY_ID := (SELECT DISTINCT G.id
                   FROM Genre G
                   WHERE G.genre = 'Comedy');

SET @DRAMA_ID := (SELECT DISTINCT G.id
                   FROM Genre G
                   WHERE G.genre = 'Drama');

SET @SHORT_ID := (SELECT DISTINCT G.id
                   FROM Genre G
                   WHERE G.genre = 'Short');

CREATE TEMPORARY TABLE ComedyClips AS (SELECT DISTINCT CG.clipId
                                       FROM ClipGenres CG
                                       WHERE CG.genreId = @COMEDY_ID);

CREATE TEMPORARY TABLE DramaClips AS (SELECT DISTINCT CG.clipId
                                      FROM ClipGenres CG
                                      WHERE CG.genreId = @DRAMA_ID);

CREATE TEMPORARY TABLE ShortClips AS (SELECT DISTINCT CG.clipId
                                      FROM ClipGenres CG
                                      WHERE CG.genreId = @SHORT_ID);

CREATE TEMPORARY TABLE CorrectGenreClipIDs AS (SELECT DISTINCT C.id
                                               FROM Clip C
                                               WHERE C.id NOT IN (SELECT DISTINCT *
                                                                        FROM ComedyClips)
                                                  AND C.id NOT IN (SELECT DISTINCT *
                                                                         FROM DramaClips)
                                                  AND C.id IN (SELECT DISTINCT *
                                                                     FROM ShortClips));

CREATE TEMPORARY TABLE EnoughClipsPersonIDs AS (SELECT DISTINCT AR.personId
                                               FROM ActingRole AR
                                               GROUP BY AR.personId
                                               HAVING COUNT(DISTINCT AR.clipId) >= 101
                                               ORDER BY NULL);

CREATE TEMPORARY TABLE EnoughClipsCorrectGenreActingRole AS (SELECT DISTINCT AR.personId, AR.clipId
                                                             FROM ActingRole AR
                                                             WHERE AR.personId IN (SELECT DISTINCT *
                                                                                   FROM EnoughClipsPersonIDs)
                                                                   AND AR.clipId IN (SELECT DISTINCT *
                                                                                     FROM CorrectGenreClipIDs));

CREATE TEMPORARY TABLE GoodRatioActingRole AS (SELECT DISTINCT ECCGAR.personId
                                               FROM EnoughClipsCorrectGenreActingRole ECCGAR
                                               GROUP BY ECCGAR.personId
                                               HAVING COUNT(DISTINCT ECCGAR.clipId) >= (60 / 100) * (SELECT DISTINCT COUNT(DISTINCT AR.clipId)
                                                                                                     FROM ActingRole AR
                                                                                                     WHERE AR.personId = ECCGAR.personId)
                                                      AND (SELECT DISTINCT COUNT(DISTINCT CC.clipId)
                                                           FROM ActingRole AR
                                                           INNER JOIN ComedyClips CC
                                                           ON AR.clipId = CC.clipId
                                                           WHERE AR.personId = ECCGAR.personId) > 2 * (SELECT DISTINCT COUNT(DISTINCT DC.clipId)
                                                                                                       FROM ActingRole AR
                                                                                                       INNER JOIN DramaClips DC
                                                                                                       ON AR.clipId = DC.clipId
                                                                                                       WHERE AR.personId = ECCGAR.personId))
                                               ORDER BY NULL;

SELECT DISTINCT P.fullName,
                (SELECT DISTINCT COUNT(DISTINCT CC.clipId)
                 FROM ActingRole AR
                 INNER JOIN ComedyClips CC
                 ON AR.clipId = CC.clipId
                 WHERE AR.personId = GRAR.personId) AS comedyClips,
                (SELECT DISTINCT COUNT(DISTINCT DC.clipId)
                 FROM ActingRole AR
                 INNER JOIN DramaClips DC
                 ON AR.clipId = DC.clipId
                 WHERE AR.personId = GRAR.personId) AS dramaClips
FROM Person P
INNER JOIN GoodRatioActingRole GRAR
ON P.id = GRAR.personId;

# k) Print the number of Dutch movies whose genre is the second most popular one.
SELECT DISTINCT COUNT(DISTINCT C.id) AS count
FROM Clip C
INNER JOIN (SELECT DISTINCT CG1.clipId
            FROM ClipGenres CG1
            INNER JOIN (SELECT DISTINCT CG.genreId, COUNT(*) AS amount
                        FROM ClipGenres CG
                        GROUP BY CG.genreId
                        ORDER BY amount DESC
                        LIMIT 1, 1) AS SMPG
            ON CG1.genreId = SMPG.genreId) TMP1
ON C.id = TMP1.clipId
INNER JOIN (SELECT DISTINCT CC.clipId
            FROM ClipCountries CC
            WHERE CC.countryId = (SELECT DISTINCT C.id
                                  FROM Country C
                                  WHERE C.country = 'Netherlands')) TMP2
ON C.id = TMP2.clipId
WHERE C.type = 'V';

# l) Print the name of the producer whose role is coordinating producer, and who has produced the highest
# number of movies with the most popular genre.
SELECT DISTINCT P.fullName
FROM Person P
INNER JOIN (SELECT DISTINCT TMP1.personId, MAX(DISTINCT TMP1.amount) AS max
            FROM (SELECT DISTINCT PR.personId, COUNT(DISTINCT PR.clipId) AS amount
                  FROM ProducerRole PR
                  WHERE PR.roleName = 'coordinating producer'
                        AND PR.clipId IN (SELECT DISTINCT CG1.clipId
                                          FROM ClipGenres CG1
                                          WHERE CG1.genreId = (SELECT DISTINCT MPG.genreId
                                                           FROM (SELECT DISTINCT CG.genreId, COUNT(*) AS amount
                                                                 FROM ClipGenres CG
                                                                 WHERE CG.genreId IS NOT NULL
                                                                 GROUP BY CG.genreId
                                                                 ORDER BY amount DESC
                                                                 LIMIT 1) AS MPG))
                  GROUP BY PR.personId) TMP1) TMP
ON P.id = TMP.personId;
