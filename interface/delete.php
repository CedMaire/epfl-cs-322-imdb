<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$error = NULL;
$start = microtime(true);

$ret = [];

try {
    $connection = open_db();
    if ($_SERVER['REQUEST_METHOD'] == "DELETE") {

        if (array_key_exists('table', $_GET) && in_array($_GET['table'], ["Clip", "Person", "Country"])) {
            switch($_GET['table']) {
                case "Clip":
                $sql = "DELETE FROM Clip WHERE id = :id";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":id", intval($_GET['id']));
                break;
                
                case "Person":
                $sql = "DELETE FROM Person WHERE fullName = :name";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":name", $_GET['name']);
                break;
                
                case "Country":
                $sql = "DELETE FROM Country WHERE country = :name";
                $stmt = $connection->prepare($sql);
                $stmt->bindParam(":name", $_GET['name']);
                break;
                
                default: return;
            }
            
            // Accessing a maybe-not-defined var is bad, because PHP itself is bad; but who cares ?
            if (! $stmt->execute()) {
                $error = $stmt->errorCode();
            } else {
                $ret['rows'] = $stmt->rowCount();
            }
        } else {
            $error = "Invalid 'table' parameter in GET arguments";
        }
    } else {
        $error = "Request type error";
    }
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

$ret["error"] = $error;
$ret["time"] = microtime(true)-$start;
echo str_replace("''''", "'", json_encode($ret));
