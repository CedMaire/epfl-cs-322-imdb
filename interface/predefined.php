<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$table_queries = [
	// PART 1
	"SELECT DISTINCT Cl.title, RT.runningTime FROM Clip Cl, RunningTime RT WHERE Cl.id = RT.clipId AND RT.countryId = (
		SELECT DISTINCT Co.id FROM Country Co WHERE Co.country = 'France') ORDER BY RT.runningTime DESC LIMIT 10;", 
	"SELECT DISTINCT Co.country, ClipsPerCountryId.count FROM Country Co, (SELECT DISTINCT RD.countryId, 
		COUNT(DISTINCT C.id) AS count FROM Clip C, ReleaseDate RD WHERE C.id = RD.clipId AND YEAR(RD.date) = 2001 
		GROUP BY RD.countryId ORDER BY NULL) ClipsPerCountryId WHERE Co.id = ClipsPerCountryId.countryId;",
	"SELECT DISTINCT G.genre, ClipsPerGenre.count FROM Genre G, (SELECT DISTINCT CG.genreId, COUNT(DISTINCT C.id) 
		AS count FROM Clip C, ReleaseDate RD, ClipGenres CG WHERE C.id = RD.clipId AND C.id = CG.clipId 
		AND RD.countryId = (SELECT DISTINCT Co.id FROM Country Co WHERE Co.country = 'USA') AND YEAR(RD.date) >= 2013 
		GROUP BY CG.genreId ORDER BY NULL) ClipsPerGenre WHERE G.id = ClipsPerGenre.genreId;", 
	"SELECT DISTINCT P.fullName, TMP1.maxClips FROM Person P INNER JOIN (SELECT DISTINCT TMP.personId, MAX(TMP.numClips) 
		AS maxClips FROM (SELECT DISTINCT AR.personId, COUNT(DISTINCT AR.clipId) AS numClips FROM ActingRole AR 
		GROUP BY AR.personId ORDER BY NULL) TMP) AS TMP1 ON P.id = TMP1.personId;",
	"SELECT DISTINCT MAX(TMP.numClips) FROM (SELECT DISTINCT COUNT(DISTINCT DR.clipId) AS numClips FROM DirectorRole DR 
		GROUP BY DR.personId ORDER BY NULL) TMP;", 
	"SELECT DISTINCT P.fullName FROM Person P WHERE EXISTS (SELECT DISTINCT 1 FROM (SELECT DISTINCT AR.personId, 
		AR.clipId  FROM ActingRole AR UNION ALL SELECT DISTINCT DR.personId, DR.clipId FROM DirectorRole DR UNION ALL 
		SELECT DISTINCT PR.personId, PR.clipId FROM ProducerRole PR UNION ALL SELECT DISTINCT WR.personId, WR.clipId 
		FROM WriterRole WR) TMP WHERE TMP.personId = P.id GROUP BY TMP.clipId HAVING COUNT(*) >= 2 ORDER BY NULL) 
		LIMIT 1000;", 
	"SELECT DISTINCT L.language FROM Language L INNER JOIN (SELECT DISTINCT CL.languageId FROM ClipLanguages CL 
		GROUP BY CL.languageId ORDER BY COUNT(DISTINCT CL.clipId) DESC LIMIT 10) IDs ON L.id = IDs.languageId;", 
	"SELECT DISTINCT P.fullName	FROM Person P	WHERE P.id IN (SELECT DISTINCT TMP.personId FROM (
		SELECT DISTINCT TMP1.personId, MAX(TMP1.maxClips)	FROM (SELECT DISTINCT AR.personId, COUNT(DISTINCT C.id) 
		AS maxClips FROM ActingRole AR, (SELECT DISTINCT C1.id FROM Clip C1 WHERE C1.type = '?') C WHERE 
		AR.clipId = C.id GROUP BY AR.personId ORDER BY NULL) TMP1) TMP);",

	// PART 2
	"CREATE TEMPORARY TABLE MinVotesClips AS (SELECT DISTINCT C.id FROM Clip C WHERE C.votes >= 100); 
		CREATE TEMPORARY TABLE MinClipsActors AS (SELECT DISTINCT AR.personId FROM ActingRole AR GROUP BY AR.personId 
		HAVING COUNT(DISTINCT AR.clipId) >= 5 ORDER BY NULL); CREATE TEMPORARY TABLE EligibleActingRole AS 
		(SELECT DISTINCT AR.personId, AR.clipId FROM ActingRole AR WHERE AR.personId IN (SELECT DISTINCT MCA.personId
		FROM MinClipsActors MCA) AND AR.clipId IN (SELECT DISTINCT MVC.id FROM MinVotesClips MVC)); CREATE TEMPORARY 
		TABLE EligibleActingRoleRank AS (SELECT DISTINCT EAR.personId, EAR.clipId, C.rank FROM EligibleActingRole EAR
		INNER JOIN Clip C ON EAR.clipId = C.id); CREATE TEMPORARY TABLE HighestRatedActingRole AS (SELECT 
		RankedClipsEligibleActingRole.personId, RankedClipsEligibleActingRole.clipId, RankedClipsEligibleActingRole.rank
		FROM (SELECT EARR.personId, EARR.clipId, EARR.rank, @CLIP_RANK := if(@CURRENT_PERSON = EARR.personId, 
		@CLIP_RANK + 1, 1)  AS clipRank, @CURRENT_PERSON := EARR.personId AS currentPerson FROM EligibleActingRoleRank 
		EARR ORDER BY EARR.personId, EARR.rank DESC) RankedClipsEligibleActingRole WHERE 
		RankedClipsEligibleActingRole.clipRank <= 3); SELECT P.fullName FROM Person P INNER JOIN (SELECT DISTINCT 
		HRAR.personId, AVG(HRAR.rank) AS average FROM HighestRatedActingRole HRAR GROUP BY HRAR.personId 
		ORDER BY average DESC LIMIT 10) TOP10 ON P.id = TOP10.personId;",
	"SELECT AVG(RankedClips.rank) AS average, RankedClips.currentDecade * 10 AS decade FROM (SELECT C.year, C.rank,
		@CLIP_RANK := if(@CURRENT_DECADE = FLOOR(C.year / 10), @CLIP_RANK + 1, 1) AS clipRank, 
		@CURRENT_DECADE := FLOOR(C.year / 10) AS currentDecade FROM Clip C WHERE C.year IS NOT NULL AND C.rank 
		IS NOT NULL ORDER BY C.year, C.rank DESC) RankedClips WHERE RankedClips.clipRank <= 100 GROUP BY decade 
		ORDER BY average DESC;",
	"SELECT DISTINCT P.fullName, C.year, C.title FROM Person P, DirectorRole DR1, Clip C WHERE P.id = DR1.personId 
		AND DR1.clipId = C.id AND C.year = (SELECT DISTINCT C.year FROM DirectorRole DR, Clip C WHERE P.id = DR.personId 
		AND DR.clipId = C.id AND C.type = 'VG' ORDER BY C.year LIMIT 1);",
	"SELECT RankedClips.year, RankedClips.title, RankedClips.clipRank AS rankInYear FROM (SELECT C.year, C.title, C.rank,
		@CLIP_RANK := if(@CURRENT_YEAR = C.year, @CLIP_RANK + 1, 1) AS clipRank, @CURRENT_YEAR := C.year AS currentYear
		FROM Clip C WHERE C.year IS NOT NULL AND C.rank IS NOT NULL ORDER BY C.year, C.rank DESC) RankedClips 
		WHERE RankedClips.clipRank <= 3;",
	"CREATE TEMPORARY TABLE EligibleDirectorRole AS (SELECT DISTINCT DR.personId FROM DirectorRole DR, Clip C1 
		WHERE DR.clipId = C1.id GROUP BY DR.personId HAVING MIN(C1.rank) >= 2 + (SELECT DISTINCT MAX(C.rank) 
		FROM WriterRole WR, Clip C WHERE DR.personId = WR.personId AND WR.clipId = C.id) ORDER BY NULL); SELECT DISTINCT 
		P.fullName FROM Person P INNER JOIN (SELECT DISTINCT EDR.personId, COUNT(DISTINCT WR.clipId) AS 
		totalWritingClips FROM EligibleDirectorRole EDR, WriterRole WR WHERE EDR.personId = WR.personId GROUP BY 
		EDR.personId HAVING totalWritingClips = (SELECT DISTINCT COUNT(DISTINCT AR1.clipId) FROM ActingRole AR1 WHERE 
		EDR.personId = AR1.personId) AND totalWritingClips = (SELECT DISTINCT COUNT(DISTINCT WR1.clipId) FROM WriterRole 
		WR1 WHERE EDR.personId = WR1.personId) ORDER BY NULL) IDCount ON P.id = IDCount.personId;",
	"CREATE TEMPORARY TABLE 2ClipsActedDirected AS (SELECT DISTINCT TMP.personId FROM (SELECT DISTINCT AR.personId, 
		AR.clipId FROM (SELECT DISTINCT AR1.personId, AR1.clipId FROM ActingRole AR1) AR INNER JOIN (SELECT DISTINCT 
		DR1.personId, DR1.clipId FROM DirectorRole DR1) DR ON AR.personId = DR.personId AND AR.clipId = DR.clipId) TMP 
		GROUP BY TMP.personId HAVING COUNT(DISTINCT TMP.clipId) >= 3); SELECT DISTINCT P.fullName FROM Person P INNER 
		JOIN 2ClipsActedDirected 2CAD ON P.id = 2CAD.personId AND P.id NOT IN (SELECT DISTINCT SP.personId FROM 
		SpousePerson SP);",
	"CREATE TEMPORARY TABLE SPSWriterRoles AS (SELECT DISTINCT * FROM WriterRole WR WHERE WR.workType = 'screenplay');
		SELECT DISTINCT P.fullName FROM Person P, SPSWriterRoles SPSWR, ProducerRole PR WHERE P.id = SPSWR.personId AND 
		SPSWR.clipId = PR.clipId GROUP BY P.id HAVING COUNT(DISTINCT PR.personId) >= 3 ORDER BY NULL;",
	"SELECT DISTINCT P.fullName, AVG(C.rank) AS average FROM Person P INNER JOIN (SELECT DISTINCT AR1.personId, 
		AR1.clipId, AR1.creditOrder FROM ActingRole AR1) AR ON P.id = AR.personId INNER JOIN (SELECT DISTINCT C1.id, 
		C1.rank FROM Clip C1 WHERE C1.rank IS NOT NULL) C ON AR.clipId = C.id WHERE AR.creditOrder <= 3 GROUP BY P.id 
		ORDER BY NULL;",
	"SELECT DISTINCT AVG(C.rank) AS average FROM Clip C INNER JOIN (SELECT DISTINCT CG1.clipId FROM ClipGenres CG1 
		INNER JOIN (SELECT DISTINCT MPG.genreId, MAX(MPG.amount) FROM (SELECT DISTINCT CG.genreId, COUNT(*) AS amount 
		FROM ClipGenres CG GROUP BY CG.genreId ORDER BY NULL) AS MPG WHERE MPG.genreId IS NOT NULL) AS TMP ON 
		CG1.genreId = TMP.genreId) TMP ON C.id = TMP.clipId WHERE C.rank IS NOT NULL;",
	"SET @COMEDY_ID := (SELECT DISTINCT G.id FROM Genre G WHERE G.genre = 'Comedy'); SET @DRAMA_ID := (SELECT DISTINCT 
		G.id FROM Genre G WHERE G.genre = 'Drama'); SET @SHORT_ID := (SELECT DISTINCT G.id FROM Genre G WHERE G.genre = 
		'Short'); CREATE TEMPORARY TABLE ComedyClips AS (SELECT DISTINCT CG.clipId FROM ClipGenres CG WHERE CG.genreId =
		@COMEDY_ID); CREATE TEMPORARY TABLE DramaClips AS (SELECT DISTINCT CG.clipId FROM ClipGenres CG WHERE CG.genreId 
		= @DRAMA_ID); CREATE TEMPORARY TABLE ShortClips AS (SELECT DISTINCT CG.clipId FROM ClipGenres CG WHERE 
		CG.genreId = @SHORT_ID); CREATE TEMPORARY TABLE CorrectGenreClipIDs AS (SELECT DISTINCT C.id FROM Clip C WHERE 
		C.id NOT IN (SELECT DISTINCT * FROM ComedyClips) AND C.id NOT IN (SELECT DISTINCT * FROM DramaClips) AND C.id IN 
		(SELECT DISTINCT * FROM ShortClips)); CREATE TEMPORARY TABLE EnoughClipsPersonIDs AS (SELECT DISTINCT 
		AR.personId FROM ActingRole AR GROUP BY AR.personId HAVING COUNT(DISTINCT AR.clipId) >= 101 ORDER BY NULL);
		CREATE TEMPORARY TABLE EnoughClipsCorrectGenreActingRole AS (SELECT DISTINCT AR.personId, AR.clipId FROM 
		ActingRole AR WHERE AR.personId IN (SELECT DISTINCT * FROM EnoughClipsPersonIDs) AND AR.clipId IN (SELECT 
		DISTINCT * FROM CorrectGenreClipIDs)); CREATE TEMPORARY TABLE GoodRatioActingRole AS (SELECT DISTINCT 
		ECCGAR.personId FROM EnoughClipsCorrectGenreActingRole ECCGAR GROUP BY ECCGAR.personId HAVING COUNT(DISTINCT 
		ECCGAR.clipId) >= (60 / 100) * (SELECT DISTINCT COUNT(DISTINCT AR.clipId) FROM ActingRole AR WHERE AR.personId = 
		ECCGAR.personId) AND (SELECT DISTINCT COUNT(DISTINCT CC.clipId) FROM ActingRole AR INNER JOIN ComedyClips CC
		ON AR.clipId = CC.clipId WHERE AR.personId = ECCGAR.personId) > 2 * (SELECT DISTINCT COUNT(DISTINCT DC.clipId)
		FROM ActingRole AR INNER JOIN DramaClips DC ON AR.clipId = DC.clipId WHERE AR.personId = ECCGAR.personId))
		ORDER BY NULL; SELECT DISTINCT P.fullName, (SELECT DISTINCT COUNT(DISTINCT CC.clipId) FROM ActingRole AR
		INNER JOIN ComedyClips CC ON AR.clipId = CC.clipId WHERE AR.personId = GRAR.personId) AS comedyClips, 
		(SELECT DISTINCT COUNT(DISTINCT DC.clipId) FROM ActingRole AR INNER JOIN DramaClips DC ON AR.clipId = DC.clipId
		WHERE AR.personId = GRAR.personId) AS dramaClips FROM Person P INNER JOIN GoodRatioActingRole GRAR ON P.id = 
		GRAR.personId;",
	"SELECT DISTINCT COUNT(DISTINCT C.id) AS count FROM Clip C INNER JOIN (SELECT DISTINCT CG1.clipId FROM ClipGenres 
		CG1 INNER JOIN (SELECT DISTINCT CG.genreId, COUNT(*) AS amount FROM ClipGenres CG GROUP BY CG.genreId ORDER BY 
		amount DESC LIMIT 1, 1) AS SMPG ON CG1.genreId = SMPG.genreId) TMP1 ON C.id = TMP1.clipId INNER JOIN (SELECT 
		DISTINCT CC.clipId FROM ClipCountries CC WHERE CC.countryId = (SELECT DISTINCT C.id FROM Country C WHERE 
		C.country = 'Netherlands')) TMP2 ON C.id = TMP2.clipId WHERE C.type = 'V';",
	"SELECT DISTINCT P.fullName FROM Person P INNER JOIN(SELECT DISTINCT TMP1.personId, MAX(DISTINCT TMP1.amount) AS max
		FROM (SELECT DISTINCT PR.personId, COUNT(DISTINCT PR.clipId) AS amount FROM ProducerRole PR WHERE PR.roleName = 
		'coordinating producer' AND PR.clipId IN (SELECT DISTINCT CG1.clipId FROM ClipGenres CG1 WHERE CG1.genreId = 
		(SELECT DISTINCT MPG.genreId FROM (SELECT DISTINCT CG.genreId, COUNT(*) AS amount FROM ClipGenres CG WHERE 
		CG.genreId IS NOT NULL GROUP BY CG.genreId ORDER BY amount DESC LIMIT 1) AS MPG)) GROUP BY PR.personId) TMP1) 
		TMP ON P.id = TMP.personId;",
];

$error = NULL;
$result = [];
$start = microtime(true);

try {
	$connection = open_db();

	if ($_SERVER['REQUEST_METHOD'] === "GET") {
		if (array_key_exists('query_id', $_GET)) {
			$index = intval($_GET['query_id']);
			if ($index >= 0 && $index < count($table_queries)) {
				$sql = $table_queries[$index];
				$stmt = $connection->prepare($sql);
				$stmt->execute();
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				$result = $stmt->fetchAll();
			} else {
				$error = "Selected query doesn't exist";
			}
		} else {
			$error = "Parameter `query_id` is not valid";
		}
	}
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

output_json($result, $error, $start);
