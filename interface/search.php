<?php
ini_set('display_errors', 'On');
ini_set('memory_limit', '8192M'); 
error_reporting(E_ALL);
header('Content-Type: application/json');

require("utils.php");

$search_queries = [];
$search_queries["actors"] = "SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM ActingRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE)";
$search_queries["directors"] = "SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM DirectorRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE)";
$search_queries["producers"] = "SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM ProducerRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE)";
$search_queries["writers"] = "SELECT DISTINCT P.id, P.fullName, B.realName, B.nickname FROM WriterRole R
    LEFT JOIN Person P ON P.id = R.personId
    LEFT JOIN Biography B ON P.id = B.personId
    WHERE MATCH (P.fullName) AGAINST ('?' IN BOOLEAN MODE)";
$search_queries["clips"] = "SELECT DISTINCT * FROM Clip WHERE MATCH (title) AGAINST ('?' IN BOOLEAN MODE)";

$error = NULL;
$start = microtime(true);

$ret = [];
$ret['results'] = [];


try {
    $connection = open_db();
    if ($_SERVER['REQUEST_METHOD'] == "GET") {
        if (array_key_exists('query', $_GET)) {
            foreach($search_queries as $table => $sql) {
                $f = function($s) { return '+'.$s; };
                // all words get a + beneath them, for BOOLEAN MODE in MATCH AGAINST
                $query = implode(' ', array_map($f, explode(' ', $_GET['query']))); 

                $sql_w_query = str_replace('?', $query, $sql);
                $stmt = $connection->prepare($sql_w_query);
                if ($stmt->execute()) {
                    $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    $ret['results'][$table] = $stmt->fetchAll();
                } else {
                    $error = $stmt->errorCode();
                    break;
                }
            }
        } else {
            $error = "Invalid 'query' parameter in GET arguments";
        }
    } else {
        $error = "Request type error";
    }
} catch(PDOException $e) {
	$error = "Connection failed: " . $e->getMessage();
} catch(Exception $e) {
	$error = "Some kind of error: " . $e->getMessage();
}

$ret["error"] = $error;
$ret["time"] = microtime(true)-$start;
echo str_replace("''''", "'", json_encode($ret));
